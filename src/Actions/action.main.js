import {
    SET_SEARCH_DATA, SET_STATUS_SEARCH,SET_ACTIVE_DATA
} from './types';
import webService from '../Utils'

export const getData = () => dispatch => {
    return webService.getData()
        .then(res => {
            dispatch({
                type: SET_SEARCH_DATA,
                payload: res
            })
            return res
        })
}

export const setStatusSearch = (status) => dispatch => {
    dispatch({
        type: SET_STATUS_SEARCH,
        payload: status
    })
}

export const setActiveData = (data) => dispatch => {
    dispatch({
        type: SET_ACTIVE_DATA,
        payload: data
    })
}