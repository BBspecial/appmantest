import React, { Component } from 'react'
import './App.css'
import Main from './Components/Main';

const COLORS = {
  Psychic: "#f8a5c2",
  Fighting: "#f0932b",
  Fairy: "#c44569",
  Normal: "#f6e58d",
  Grass: "#badc58",
  Metal: "#95afc0",
  Water: "#3dc1d3",
  Lightning: "#f9ca24",
  Darkness: "#574b90",
  Colorless: "#FFF",
  Fire: "#eb4d4b"
}

class App extends Component {
  // componentDidMount = () => {


  //   fetch('http://localhost:3030/api/cards')
  //     .then(function (response) {
  //       return response.json();
  //     })
  //     .then(function (myJson) {
  //       console.log(myJson);
       
  //     });
  // }

  render() {
    return (
      <div className="App">
        <Main />
      </div>
    )
  }
}

export default App
