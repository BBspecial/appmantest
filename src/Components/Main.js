import React, { Component } from 'react'
import '../App.css'
import Search from './Search'
import { setStatusSearch } from '../Actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
class Main extends Component {
    state = { data: [], searchStatus: true }

    render() {
        return (
            <div className="container">
                <div className="header">My Pokédex</div>
                <div onClick={e => {
                    console.log('click 1')
                }}>
                    {!this.props.statusSearch &&

                        <div style={{
                            height: 620,
                            overflowY: 'auto',
                            flexDirection: 'column',
                        }}>
                            {this.props.activeData.map((e, i) => {
                                return <div key={e.id} >
                                    <div style={{ backgroundColor: '#d4d4d4', display: 'flex', justifyContent: 'center', margin: 5 }}>
                                        <img src={e.imageUrl} style={{ height: 200 }} alt={e.name} />
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around' }}>
                                            {e.name}
                                            <a>HP</a>
                                            <a>STR</a>
                                            <a>WEAK</a>
                                        </div>
                                    </div>
                                </div>
                            })}
                        </div>}
                    {this.props.statusSearch &&
                        <Search />}
                </div>
                <div style={{ backgroundColor: '#ec5656', width: '100%', height: 80 }}
                    onClick={e => {
                        console.log('clicked ', !this.state.searchStatus)
                        this.setState({ searchStatus: !this.state.searchStatus })
                        this.props.setStatusSearch(true)
                    }}>
                    <div>+</div>
                </div>
                {/* </div> */}
            </div>

        )
    }
}
const mapStateToProps = ({ mainReducer }) => {
    const { searchData, statusSearch, activeData } = mainReducer
    return { searchData, statusSearch, activeData }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    setStatusSearch

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)