import React, { Component } from 'react'
import '../App.css'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getData, setStatusSearch, setActiveData } from '../Actions'

class Search extends Component {
    state = { data: [] }
    componentDidMount = () => {
        this.props.getData()
    }

    render() {
        return (
            <div className="container" >

                <div style={{ backgroundColor: '#ffffff' }}>
                    <input
                        style={{ width: 600 }}
                        onChange={e => {
                            console.log('input change')
                        }} />
                    <div style={{
                        height: 620,
                        widht: 400,
                        overflowY: 'auto'
                    }}>
                        {this.props.searchData.filter(e => {
                            if (this.props.activeData.length === 0) {
                                return true
                            } else {
                                return !this.props.activeData.filter(x => {
                                    return x.id === e.id
                                }).length
                            }

                        }).
                            map((e, i) => {
                                return <div key={i + e.id} >
                                    <div style={{  backgroundColor: '#d4d4d4' ,display: 'flex', justifyContent: 'space-around', margin: 5 }}>
                                        <img src={e.imageUrl} style={{ height: 200 }} alt={e.name} />
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around' }}>
                                            {e.name}
                                            <a>HP</a>
                                            <a>STR</a>
                                            <a>WEAK</a>
                                        </div>
                                        <a onClick={ee => {
                                            if (this.props.activeData.length) {
                                                let temp = this.props.activeData
                                                temp.push(e)
                                                console.log('temp ', temp)
                                                this.props.setActiveData(temp)
                                            } else {
                                                this.props.setActiveData([e])
                                            }
                                            this.props.setStatusSearch(false)
                                        }
                                        }>add</a>
                                    </div>


                                </div>
                            })}
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ mainReducer }) => {
    const { searchData, statusSearch, activeData } = mainReducer
    console.log('activeData', activeData)
    return { searchData, statusSearch, activeData }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getData,
    setStatusSearch,
    setActiveData

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Search)