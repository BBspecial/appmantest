import {
    SET_SEARCH_DATA,
    SET_STATUS_SEARCH,
    SET_ACTIVE_DATA
} from '../Actions/types';

const initialState = {
    statusSearch: false,
    activeData: [],
    searchData: []
}

export default (state = initialState, action) => {
    const { payload } = action
    switch (action.type) {

        case SET_SEARCH_DATA:
            return {
                ...state,
                searchData: payload
            }
        case SET_STATUS_SEARCH:
            return {
                ...state,
                statusSearch: payload
            }
        case SET_ACTIVE_DATA:
            return {
                ...state,
                activeData:payload
            }
        default:
            return state
    }
}